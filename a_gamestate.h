#pragma once

#include "game.h"

class Game;
class GameState
{
protected:
	int		m_id;
	Game&		m_current_game;

public:
	bool	m_looping;

	GameState(int id, Game& g) : m_id(id), m_current_game(g)
	{
	}

	virtual ~GameState()
	{
		//cleanup();
	}

	int get_id() { return m_id; }

	virtual void draw() = 0;
	virtual void input() = 0;
	virtual void update() = 0;
	virtual void cleanup() = 0;
};