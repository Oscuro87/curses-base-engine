#pragma once

#include <stack>
#include <vector>
#include <panel.h>
#include "a_gamestate.h"
#include "defines.h"

class Game
{
private:
	std::stack<GameState*>	states;
	std::vector<GameState*> saved_states;
	GameState*				current_state;
	bool					m_running;

public:
	Game()
	{
	}

	~Game()
	{
		halt();
		for(int i = 0 ; i < states.size() ; ++i)
		{
			delete states.top();
			states.pop();
		}
	}

	void launch(GameState* first_state)
	{
		current_state = 0;
		m_running = true;

		initscr();
		resize_term(TERM_SIZE_Y, TERM_SIZE_X);

		cbreak();
		noecho();
		keypad(stdscr, TRUE);
		curs_set(0);
		start_color();

		if(first_state == 0)
			mvprintw(1,1,"The first gamestate could not be found, exiting...");
		else
		{
			push_state(first_state);
			current_state = first_state;
			game_loop();
		}

		nocbreak();
		getch();
		endwin();
	}

	void game_loop()
	{
		while(m_running)
		{
			while(current_state->m_looping)
			{
				current_state->draw();
				current_state->input();
				current_state->update();
			}
			current_state->cleanup();
			pop_state(current_state);
		}
	}

	void halt()
	{
		m_running = false;
	}

	void save_state(GameState* state)
	{
		GameState* replace = NULL;
		if( ( replace = get_saved_state_by_id(state->get_id()) ) == NULL)
			saved_states.push_back(state);
		else
		{
			delete replace;
			replace = state;
		}
	}

	GameState* get_saved_state_by_id(int id)
	{
		std::vector<GameState*>::iterator it;
		for(it = saved_states.begin() ; it < saved_states.end() ; ++it)
			if( id == (*it)->get_id() ) return (*it);
		return NULL;
	}

	void push_state(GameState* newState)
	{
		GameState* retrieve = get_saved_state_by_id(newState->get_id);
		
		if(retrieve == NULL)
		{
			states.push(newState);
			current_state = newState;
		}
		else
		{
			current_state = retrieve;
			delete newState;
		}
	}

	GameState* pop_state(bool save)
	{
		if(states.size() == 0)
			return NULL;
		else
		{
			GameState* popped = states.top();
			if(save)
				save_state(popped);
			else
				delete popped;
			states.pop(); // removes the slot in the stack even tho the obj is deleted.

			if(states.size() != 0)
				current_state = states.top();
			else
				halt();
			return popped;
		}
	}
};