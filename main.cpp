#include "game.h"
#include "state_test.h"

int main(void)
{
	Game* g = new Game();
	// Create your GameState and pass it to the launcher!
	State_Test st(*g);
	g->launch(&st);
	delete g;
}