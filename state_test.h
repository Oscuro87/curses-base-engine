#pragma once

#include "a_gamestate.h"
#include "state_switch.h"

class State_Test : public GameState
{
private:
public:
	State_Test(Game& g) : GameState(-1, g)
	{
	}

	virtual ~State_Test()
	{
		cleanup();
	}

	virtual void draw()
	{
		clear();
		mvprintw(0,0,"Test Game State");
	}

	virtual void input()
	{
		halfdelay(1);
		char key;

		if( (key = getch()) != ERR)
		{
			if(key == 's' | key == 'S')
			{
				State_Switch* ss = new State_Switch(m_current_game);
				m_current_game.push_state(ss);
			}
			else
				m_looping = false;
		}

		cbreak();
	}

	virtual void update()
	{
	}

	virtual void cleanup()
	{
		clear();
		mvprintw(5,5, "Deleting test game state... (Press a key to proceed...)");
		getch();
	}
};