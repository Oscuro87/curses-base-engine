#pragma once

#include "a_gamestate.h"

class State_Switch : public GameState
{
private:
	int		aCount;
public:
	State_Switch(Game& g) : GameState(0, g)
	{
		aCount = 1;
	}

	virtual ~State_Switch()
	{
		cleanup();
	}

	virtual void draw()
	{
		clear();
		mvprintw(0,0,"Test Game State Switch");

		mvprintw(5,1,"");
		for(int i = 0; i < aCount ; ++i)
			printw("a");
		++aCount;
	}

	virtual void input()
	{
		halfdelay(1);
		
		if(getch() != ERR)
		{
			m_looping = false;
		}

		nocbreak();
	}

	virtual void update()
	{
	}

	virtual void cleanup()
	{
		const char* msg = "Deleting test game state switch... (Press a key to proceed...)";
		int nlines = 5, ncols = strlen(msg) + 4, begy = 5, begx = 5;
		WINDOW* panelwin = newwin(nlines, ncols, begy, begx);
		PANEL* p = new_panel(panelwin);
		box(panelwin, 0, 0);
		mvwprintw(panelwin, 0, 2, "MESSAGE!");
		mvwprintw(panelwin, 2, 2, msg);
		update_panels();
		doupdate();

		getch();
	}
};